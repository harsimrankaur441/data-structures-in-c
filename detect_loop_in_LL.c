#include<stdio.h>
#include<stdlib.h>

struct node{
    int data;
    struct node * ptr;
};

void add_node(struct node **head_ref,int data){
        if(*head_ref == NULL){
            struct node *new_node = (struct node *)malloc(sizeof(struct node));
            new_node->data = data;
            new_node->ptr = NULL;
            *head_ref = new_node;
        }
        else{
            struct node *temp = *head_ref;
            while(temp->ptr){
                temp = temp->ptr;
            }
            struct node *new_node = (struct node *)malloc(sizeof(struct node));
            new_node->data = data;
            new_node->ptr = NULL;
            temp->ptr= new_node;

        }
}


void print_list(struct node *head){
        while(head){
            printf("\n%d\n",head->data);
            head = head->ptr;
        }
        
}
struct node* detect_loop(struct node *head){
    struct node *slow = head,*fast=head;
    printf("\nhead data -- %d\n",head->data);
    while(slow && fast && fast->ptr){
        slow= slow->ptr;
        fast = fast->ptr->ptr;
        printf("\nSlow and fast data after each iteration -- %d %d\n",slow->data,fast->data);
        if(slow == fast){
            break;
        } 
    }
    printf("\nwhere slow and fast matched -- %d\n",slow->data);
    return slow;
}

struct node * remove_loop(struct node *head,struct node *loop_node){
    struct node *ptr1 = head,*ptr2 = loop_node;
    printf("\n ptr1 and ptr2 data -- %d %d\n",ptr1->data,ptr2->data);
    while(ptr1->ptr != ptr2->ptr){
        ptr2 = ptr2->ptr;
        ptr1 = ptr1->ptr; 
    }
    ptr2->ptr = NULL;
    //printf("\n Start point of loop -- %d \n",ptr2->ptr->data);

}

 
int main(){
    int num,ch,n;
    struct node n1;
    struct node *head = NULL , *temp;
    printf("Want to add element \n");
    scanf("%d",&ch);
     while(ch == 1){
        printf("Enter element \n");
        scanf("%d",&num);
        add_node(&head,num);
        printf("Want to dd element or not\n");
        scanf("%d",&ch);
    } 
    print_list(head);
    printf("\ndata %d %d \n",head->ptr->ptr->ptr->ptr->ptr->data,head->ptr->ptr->data);
    head->ptr->ptr->ptr->ptr->ptr = head->ptr->ptr; // Very important that last ptr will note move to next node but is the ptr field of node that we reach through upto lasr ptr
    //print_list(head);
    struct node *loop_node = detect_loop(head);
    remove_loop(head,loop_node);
    print_list(head);

}