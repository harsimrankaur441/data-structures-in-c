#include<stdio.h>
#include<string.h>

void reverse(char *start,char *end){
    char c;
    while(start<end){
        c= *start;
        *start++ = *end;
        *end-- = c;

    }
    
}
void reverse_words(char *s){
    char *words_begin = s;
    char *temp = s;
    //Reverse the individual words
    while(*temp){
        temp++;
        if(*temp == ' '){
            reverse(words_begin,temp-1);
            words_begin = temp +1;

        }else if(*temp =='\0'){
                reverse(words_begin,temp-1);
        }
    }
    reverse(s,temp-1); //Reverse the entire string

}
int main(){
    char s[]="I like programming very much";
    char *temp = s;
    printf("%s",temp);
    reverse_words(temp);
    printf("%s",s);



}
