#include<stdio.h>
#include<stdlib.h>
struct node{
    int data;
    struct node *ptr;
};

void traverse_list(struct node *ptr_to_head ){
     struct node *temp_head = ptr_to_head;
     while(temp_head){
         printf("\n%d",temp_head->data);
         temp_head= temp_head->ptr;
     }
}

void insert_list(struct node **ptr_to_head,int n){
    if(*ptr_to_head == NULL){
        printf("First element it is --");
        struct node *temp = (struct node *)malloc(sizeof(struct node));
        temp->data = n;
        temp->ptr=NULL;
        *ptr_to_head = temp;
       
    }
    else{
        struct node *temp_head = *ptr_to_head;
        while(temp_head->ptr){
            temp_head = temp_head->ptr;
        }
        struct node *temp = (struct node *)malloc(sizeof(struct node));
        temp->data = n;
        temp->ptr=NULL;
        temp_head->ptr = temp;
    }

}
struct node *sort_012(struct node *head_ref){
    struct node *p0,*p1,*p2,*P0,*P1,*P2;
    p0=p1=p2=P0=P1=P2 =NULL;
    while(head_ref){
        if(head_ref->data == 0){
            if(p0 == NULL){
                P0 = head_ref;
                p0=P0;
            }else{
                p0->ptr = head_ref;
                p0 = p0->ptr;
            }
        }else if(head_ref->data == 1){
            if(p1 == NULL){
                P1= head_ref;
                p1 = P1;
            }else{
               p1->ptr = head_ref;
                p1 = p1->ptr ;
            }
        }else{
            if(p2 == NULL){
                P2= head_ref;
                p2 = P2;
            }else{
                p2->ptr = head_ref;
                p2 = p2->ptr;
            }
        }
        head_ref = head_ref->ptr;
    }
    p0->ptr = P1;
    p1->ptr = P2;
    p2->ptr =NULL;
    return P0;
}
/* good implementation using count sort 
void sortList(Node *head)  
{ 
    int count[3] = {0, 0, 0}; 
    Node *ptr = head;  
  
    while (ptr != NULL)  
    {  
        count[ptr->data] += 1;  
        ptr = ptr->next;  
    }  
    int i = 0;  
    ptr = head;  
    while (ptr != NULL)  
    {  
        if (count[i] == 0)  
            ++i;  
        else
        {  
            ptr->data = i;  
            --count[i];  
            ptr = ptr->next;  
        }  
    }  
}  
*/
/*void sort_012(struct node *head){
    int c0,c1,c2,i;
    struct node *temp = head;
    c0=c1=c2=i=0;
    while(head){
        if(head->data == 0){c0++;}
        else if(head->data == 1){c1++;}
        else if(head->data == 2){c2++;}
        head = head->ptr;
    }
    printf("\n %d %d %d \n", c0,c1,c2);
    head = temp;
    
        while(i<c0){
            head->data = 0;
            head = head->ptr;
            i++;
        }
        i=0;
        while(i<c1){
            head->data = 1;
            head = head->ptr;
            i++;
        }
        i=0;
         while(i<c2){
            head->data = 2;
            head = head->ptr;
            i++;
        }

}
*/
int main(){
    int n,choice;
    struct node *head= NULL;
    
    do{
        printf("Enter element --\n");
        scanf("%d",&n);
        insert_list(&head,n);
        printf("Do u want to add more elements (press 1 then)---\n");
        scanf("%d",&choice);
    }while(choice);
    printf("Displaying elements---\n");
    traverse_list(head);
    //sort_012(head);
    head = sort_012(head);
    printf("After sorting elements---\n");
    traverse_list(head);
}