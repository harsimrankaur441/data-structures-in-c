#include<stdio.h>
int main(){
    int t,n,s,temp;
    printf("Test cases --\n");
    scanf("%d",&t);
    while(t--){
        printf("Array size --\n");
        scanf("%d",&n);
        int a[n];
        for(int i=0;i<n;i++){
            scanf("%d",&a[i]);
        
        }
        //Sorting
        for(int i=n-1 ;i>=0;i--){
            int max_ptr = 0;
            for(int j=1;j<=i;j++){
                if(a[max_ptr] < a[j]){
                    max_ptr = j;
                }
            }
            temp = a[max_ptr];
            a[max_ptr] = a[i];
            a[i] = temp;
        }
        printf("\nAfter Sorting\n");
        for(int i=0;i<n;i++){
            printf("%d\n",a[i]);
        }
        //Counting the triplets
        
        for(int i=n-1 ;i>=2;i--){
            int j = i-1 , k= 0;
            while(k<j){
                if(a[i] > a[k]+a[j]){
                        k++;
                }
                if(a[i] < a[k]+a[j]){
                        j--;
                }
                if(a[i] == a[k]+a[j]){
                        printf("Triplet found at - %d + %d = %d",a[k],a[j],a[i]);

                        while(--j && a[i] == a[k]+a[j] && k<j ){
                            printf("Triplet found at - %d + %d = %d",a[k],a[j],a[i]);
                        }
                        while(++k && a[i] == a[k]+a[j] && k<j){
                            printf("Triplet found at - %d + %d = %d",a[k],a[j],a[i]);
                        }
                }
            }
        }
       
    }

}