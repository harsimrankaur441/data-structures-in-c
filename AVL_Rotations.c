#include<stdio.h>
#include<stdlib.h>

 
 struct node{
     int data;
     struct node *left;
     struct node *right;
     int height;
 };
int get_balance(struct node *root){
    if(root == NULL){return 0;}
    return height(root->left) - height(root->right);

}

struct node * right_rotate(struct node *root){
    struct node *y= root->left;
    struct node *T1= y->right;
    y->right = root ;
    root->left = T1;
    //Update heights
    root->height = max(height(root->left), height(root->right)) + 1;  
    y->height = max(height(y->left),height(y->right)) + 1;  
    //Return new root
    return y;

}
struct node * left_rotate(struct node * root){
    struct node *y= root->right;
    struct node *T2 = root->right->left;
    root->right->left = root; // or y->left = root
    root->right = T2; 
    // Update heights  
    root->height = max(height(root->left),height(root->right)) + 1;  
    y->height = max(height(y->left), height(y->right)) + 1;  
    // Return new root  
    return y;
}

int max(int a,int b){
    return (a>b)? a : b;
}
void inorder(struct node *root){
    if(root != NULL){
        inorder(root->left);
        printf("\n%d",root->data);
        inorder(root->right);
    }
}
void preorder(struct node *root){
    if(root != NULL){
        printf("\n%d",root->data);
        preorder(root->left);
        preorder(root->right);
    }
}
void postorder(struct node *root){
    if(root != NULL){
        postorder(root->left);
        postorder(root->right);
        printf("\n%d",root->data);
    }
}

int height(struct node *n){
    if(n == NULL){
        return 0;
    }
    return n->height;
}
struct node * insert_AVL(struct node *root,int data){
    if(root == NULL){
        struct node *temp = (struct node *)malloc(sizeof(struct node));
        temp->left =NULL;
        temp->right =NULL;
        temp->data = data;
        temp->height= 1;
        return temp;
    }
    else{
        if(data < root->data){
            root->left=insert_AVL(root->left,data);
        }
        if(data > root->data){
            root->right=insert_AVL(root->right,data);
        }
        if(data == root->data){
            return root;
        }

    }
    root->height= 1+ max(height(root->left),height(root->right));
    int balance_factor = get_balance(root);
    printf("\nRoot data ,its height and balance factor--- %d %d %d",root->data,root->height,balance_factor);

    if(balance_factor > 1 && data < root->left->data){
            return right_rotate(root);
    }

    if(balance_factor < -1 && data > root->right->data){
            return left_rotate(root);
    }
    if(balance_factor > 1 && data > root->left->data){
            root->left= left_rotate(root->left);
            return right_rotate(root);
    }
    if(balance_factor < -1 && data < root->right->data){
            root->right= right_rotate(root->right);
            return left_rotate(root);
    }
    

   return root;
}









 int main(){
     struct node *root =NULL;
     root = insert_AVL(root,70);
     root =insert_AVL(root,60);
     //inorder(root);
     root = insert_AVL(root,30);
     //inorder(root);
     root = insert_AVL(root,40);
     //inorder(root); 
     root = insert_AVL(root,50);
     inorder(root);
     root = insert_AVL(root,25);
     printf("\nInorder \n--");
     inorder(root);
     printf("\nPreorder \n--");
     preorder(root);
     printf("\nPostorder \n--");
     postorder(root);
 }