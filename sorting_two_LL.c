#include <stdlib.h>
#include<stdio.h>

struct node {
  int data;
  struct node *ptr;
};



//-- Sort without recursion ----
struct node* merge(struct node* h1,struct node* h2) 
{ 
    if (!h1) 
        return h2; 
    if (!h2) 
        return h1; 
         
    // start with the linked list 
    // whose head data is the least 
    if (h1->data < h2->data) { 
        h1->ptr = merge(h1->ptr, h2); 
        return h1; 
    } 
    else { 
        h2->ptr = merge(h1, h2->ptr); 
        return h2; 
    } 
} 

void sort_LL(struct node *head1,struct node *head2){
        struct node *prev = NULL , *p2 = NULL;
        while(head1 || head2){
            if(head1 == NULL){
                if(head2 != NULL){
                    while(head2){
                        prev->ptr = head2;
                        prev = prev->ptr;
                        head2 = head2->ptr;
                    }
                    return;
                }
            }
            else if(head2 == NULL){
                while(head1){
                    head1 = head1->ptr;
                }
                return;
            }
            else if(head1->data > head2->data){
                p2 = head2->ptr;
                head2->ptr = head1;
                if(prev != NULL){
                    prev->ptr = head2;
                }
                prev = head2;
                head2 = p2;

            }
            else if(head1->data <= head2->data){
                prev = head1;
                head1 = head1->ptr;
            }
        }
        
}




void add_node(struct node **head_ref,int data){
   if(*head_ref == NULL){
            struct node *new_node = (struct node *)malloc(sizeof(struct node));
            new_node->data = data;
            new_node->ptr = NULL;
            *head_ref = new_node;
        }
        else{
            struct node *temp = *head_ref;
            while(temp->ptr){
                temp = temp->ptr;
            }
            struct node *new_node = (struct node *)malloc(sizeof(struct node));
            new_node->data = data;
            new_node->ptr = NULL;
            temp->ptr= new_node;

        }
}
void print_list(struct node *head){
        while(head){
            printf("\t%d",head->data);
            head = head->ptr;
        }
}
int main(){
  int n,data,ch=1;
  struct node *head1 =NULL, *head2 = NULL , *result =NULL;
  while(ch){
        printf("\nEnter your elemnent --\n");
        scanf("%d",&data);
        add_node(&head1, data);
        printf("\nDo u want to continue--\n");
        scanf("%d",&ch);
       
  }
  
  
  ch=1;
  while(ch){
        printf("\nEnter your elemnent --\n");
        scanf("%d",&data);
        add_node(&head2, data);
        printf("\nDo u want to continue--\n");
        scanf("%d",&ch);
       
  }
  printf("First list --\n");
  print_list(head1);
  printf("Second list --\n");
  print_list(head2);
  
  /*sort_LL(head1,head2);
  printf("\nAfter sorting without recursion--\n");
  print_list(head1);*/
  
  head1 = merge(head1,head2);
  printf("\nAfter sorting with recursion  --\n");
  print_list(head1);
  
}
