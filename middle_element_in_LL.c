#include <stdlib.h>
#include<stdio.h>
#define elements 21

struct node {
  int data;
  struct node *ptr;
};


// creation and addition of node

void add_node(struct node **head_ref,int data){
  struct node *new_node = (struct node *)malloc(sizeof(struct node));
  new_node->data = data;
  new_node->ptr = *head_ref;
  *head_ref = new_node;
}

int get_middle(struct node *head ){
  struct node *slow = head,*fast = head;
  printf("\n--Head data --  %d\n",slow->data,fast->data);
  while(fast != NULL && fast->ptr != NULL){
    slow = slow->ptr;
    fast = fast->ptr->ptr;
  }
  printf("The middle element is [%d]\n\n", slow->data);  
  return slow->data;
}

int main(){
  int n,data;
  struct node *head =NULL;
  printf("\n Number of elements ---\n");
  scanf("%d",&n);
  for(int i=0;i<n;i++){
    printf("\nEnter your %d elemnent --\n",i+1);
    scanf("%d",&data);
    add_node(&head, data);
  }
  int middle =get_middle(head);
  printf("\n%d \n",middle);
}

// Creation of an array using malloc ---
/*
int *ptr , *temp , *fast;
 ptr = malloc(elements * sizeof(*ptr)); 
    int i=0;
     temp = ptr;

    while(i<elements)
    if (temp != NULL) {
      *(temp + i) = 1+ i; 
      //printf("Value of the ith integer is %d",*(temp+i));
      i++;
    }
    i=0;
    while(i<elements){
      printf("\n %d\n",*(temp+i));
      i++;
    }
    temp = ptr;
    fast = ptr;
     i=0;
     while(i<elements/2){
       temp =temp + 1;
       printf("\n %d\n",*fast);
       fast = fast+ 2;
        printf("\n %d\n",*temp);
        i++;
     }
     printf("\n--Middle eleemnt is %d \n",*temp);

}*/


