#include<stdio.h>
#include<stdlib.h>

struct node{
    int data;
    struct node *ptr;
};

void add_node(struct node **head_ref,int data){
  struct node *new_node = (struct node *)malloc(sizeof(struct node));
  new_node->data = data;
  new_node->ptr = *head_ref;
  *head_ref = new_node;
}

void print_list(struct node *head){
    struct node *temp = head;
    while(temp){
        printf("\n%d\n",temp->data);
        temp= temp->ptr;
    }
}

struct node * reverse_list(struct node *head){
    struct node *p =head , *q =NULL ,*r=NULL;
    while(p!=NULL){
        r = p->ptr;
        p->ptr = q;
        q=p;
        p=r;
    }
    return q;
}

int main(){
    int n,data;
    struct node *head =NULL;
    printf("\n Number of elements ---\n");
    scanf("%d",&n);

for(int i=0;i<n;i++){
    printf("\nEnter your %d elemnent --\n",i+1);
    scanf("%d",&data);
    add_node(&head, data);
  }
  printf("Before\n");
  print_list(head);
  printf("After \n");
  head = reverse_list(head);
  print_list(head);

}