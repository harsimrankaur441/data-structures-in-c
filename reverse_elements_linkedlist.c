#include<stdio.h>
#include<stdlib.h>
struct node{
    int data;
    struct node *ptr;
};
int top=-1,stack[100];
void traverse_list(struct node **ptr_to_head ){
     struct node *temp_head = *ptr_to_head;
     while(temp_head){
         
         printf("\n%d",temp_head->data);
         temp_head= temp_head->ptr;
     }
}
void reverse_elements(struct node **ptr_to_head ){
     struct node *temp_head = *ptr_to_head;
     while(temp_head){
         //Update the list
         temp_head->data = stack[top--];
         temp_head= temp_head->ptr;
     }
}



void insert_list(struct node **ptr_to_head,int n){
    if(*ptr_to_head == NULL){
        printf("First element it is --");
        struct node *temp = (struct node *)malloc(sizeof(struct node));
        temp->data = n;
        temp->ptr=NULL;
        *ptr_to_head = temp;
       
    }
    else{
        struct node *temp_head = *ptr_to_head;
        while(temp_head->ptr){
            temp_head = temp_head->ptr;
        }
        struct node *temp = (struct node *)malloc(sizeof(struct node));
        temp->data = n;
        temp->ptr=NULL;
        temp_head->ptr = temp;
    }
     stack[++top]= n;

}

int main(){
    int n,choice;
    struct node *head= NULL;
    
    do{
        printf("Enter element --\n");
        scanf("%d",&n);
        insert_list(&head,n);
        printf("Do u want to add more elements (press 1 then)---\n");
        scanf("%d",&choice);
    }while(choice);
    printf("Displaying elements---\n");
    traverse_list(&head);
    printf("After reversing the elements of linked list --");
    reverse_elements(&head);
    printf("Displaying elements---\n");
    traverse_list(&head);


}