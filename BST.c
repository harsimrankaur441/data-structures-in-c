#include<stdio.h>
#include<stdlib.h>
struct node{
    int data;
    struct node *lp, *rp;
};

struct node* insert_BST(struct node *root,int key){
    if(root == NULL){
        struct node *temp =(struct node *)malloc(sizeof(struct node));
        temp->data = key;
        temp->lp = NULL;
        temp->rp= NULL;
        return temp;
    }
    else{
        if(key < root->data){
            root->lp = insert_BST(root->lp,key);
        }
        else if(key > root->data){
            root->rp = insert_BST(root->rp,key);
        }
        
    }
    return root;
}
void inorder(struct node *root){
    if(root!=NULL){
        inorder(root->lp);
        printf("\n%d",root->data);
        inorder(root->rp);

    }
}

char flag='r';
void delete(struct node *root,struct node *prev,int key){
    printf("\nroot data -- %d",root->data);
    
    if(key > root->data){
        prev = root;
        flag='r';
        //printf("\nMove to r %d %d",root->data, prev->data);
        delete(root->rp ,prev,key);
    }
    if(key < root->data){
        prev = root;
        flag='l';
        //printf("\nMove to l %d",root->data);
        delete(root->lp ,prev,key);
    }
    if(key == root->data){
        if(root->lp == NULL && root->rp == NULL){
            if(flag == 'l'){prev->lp = NULL;}
            if(flag =='r'){prev->rp = NULL;}
        }
        /*if(root->lp != NULL && root->rp != NULL){
            int largest = find_del_max(root->lp);
            root->data = largest;
        }*/
        if(root->lp != NULL && root->rp == NULL){

            printf("Left ---");
            if(flag =='r'){
                printf("--- R ---");

                prev->rp=root->lp;
            }

            if(flag =='l'){
                printf("--- L ---");
                prev->lp=root->lp;
            }
                

            
            
        }
        if(root->lp == NULL && root->rp != NULL){
          printf("Right ---");
            if(flag =='r'){
                printf("--- R ---");

                prev->rp=root->rp;
            }

            if(flag =='l'){
                printf("--- L ---");
                prev->lp=root->rp;
            }
                
            
        }
        if(root->lp != NULL && root-> rp != NULL){
            if(root->lp){
                struct node *temp = root->lp, *prev=NULL;
                prev = root; 
                while(temp->rp != NULL){
                    prev = temp;
                    temp = temp->rp;
                }
                int largest = temp->data;
                delete(root,prev,temp->data);
                root->data = largest;
            }

        }
        
        
    }

    
}
int main(){
    struct node *root= NULL,*prev=NULL;
    root = insert_BST(root,150);
    insert_BST(root,30);
    insert_BST(root,20);
    insert_BST(root,240);
    insert_BST(root,270);
    insert_BST(root,260);
    insert_BST(root,180);
        insert_BST(root,90);
    insert_BST(root,160);
    insert_BST(root,60);
    insert_BST(root,80);

    //inorder(root);
    delete(root,prev,150);
    inorder(root);

    delete(root,prev,240);
    inorder(root);
    delete(root,prev,20);
    inorder(root);
    delete(root,prev,60);
    inorder(root);
    delete(root,prev,80);
    inorder(root);
    delete(root,prev,90);
    inorder(root);

}